#!/usr/bin/python

from __future__ import division

from numpy import zeros, tile

from scikits.audiolab import play
from scikits.samplerate import resample

from scipy.io import wavfile

import pylab as plt

def main():
	orig_rate, orig_data = wavfile.read("fart.wav")
	orig_data = orig_data[:44100]

	orig_data = tile(orig_data, 2)

	result = zeros( (len(orig_data)) )

	for i in xrange(0, len(result), 1050):
		resample_rate = -(i/1050) * (2/84) + 2
		print resample_rate

		resampled_i = int(i * resample_rate)

		new_fart = resample(orig_data, resample_rate, 'linear')[resampled_i:resampled_i + 1050]

		if len(new_fart) < 1050:
			new_fart = tile(new_fart, (1050 // len(new_fart)) + 1)

		result[i:i+1050] = new_fart[:1050]

	play(result / 2**15)

if __name__ == '__main__':
	main()