#!/usr/bin/python

from __future__ import division

from numpy import linspace, sin, pi, int16, array, zeros, take
from numpy.fft import rfft

from scipy.io import wavfile

from scikits.audiolab import play
from scikits.samplerate import resample

from pylab import plot, show, subplot

class SinSynthesizer(object):
	"""Outputs a simple sin wave at the specified tone"""
	def __init__(self, rate, sample_length):
		super(SinSynthesizer, self).__init__()
		self._t = linspace(0, sample_length / rate, sample_length)

		self._freq = 0
		self._amp = 0

	def freq():
	    doc = "The freq property."
	    def fget(self):
	        return self._freq
	    def fset(self, value):
	        self._freq = value
	    def fdel(self):
	        del self._freq
	    return locals()
	freq = property(**freq())

	def amp():
	    doc = "The amp property."
	    def fget(self):
	        return self._amp
	    def fset(self, value):
	        self._amp = value
	    def fdel(self):
	        del self._amp
	    return locals()
	amp = property(**amp())

	def generate(self):
		return (sin(2 * pi * self.freq * self._t) * self.amp).astype(int16)

class FartSynthesizer(object):
	"""Create fart sounds of different frequencies"""

	orig_fart = wavfile.read("fart.wav")[1][:44100]

	def __init__(self, rate, sample_length):
		super(FartSynthesizer, self).__init__()
		self.rate = rate
		self.sample_length = sample_length
		
		self._index = 0
		self._max = max(self.orig_fart)
	
	def freq():
	    doc = "The freq property."
	    def fget(self):
	        return self._freq
	    def fset(self, value):
	        self._freq = value
	    def fdel(self):
	        del self._freq
	    return locals()
	freq = property(**freq())

	def amp():
	    doc = "The amp property."
	    def fget(self):
	        return self._amp
	    def fset(self, value):
	        self._amp = value
	    def fdel(self):
	        del self._amp
	    return locals()
	amp = property(**amp())

	def generate(self):
		base_freq = 200

		if self.freq == 0:
			return zeros( (self.sample_length) )

		resample_rate = base_freq / self.freq
		resampled_index = int(self._index * resample_rate)

		new_fart = resample(self.orig_fart, resample_rate, 'linear')
		sample = take(new_fart, range(resampled_index, resampled_index + self.sample_length), mode='wrap')

		sample *= self.amp / self._max

		self._index += self.sample_length

		return sample

# def note(freq, length, amp=1, rate=44100):
# 	t = linspace(0, length, length * rate)
# 	data = sin(2 * pi * freq * t) * amp
# 	return data.astype(int16)

def find_peaks(fft):
	prev = fft[0]
	peaks = []
	going_up = True
	threshold = max(fft) / 100
	for i, f in enumerate(fft):
		if prev - f > threshold and going_up:
			peaks.append(i - 1)
			going_up = False
		elif f > prev:
			going_up = True

		prev = f
		
	return peaks

def main():
	rate, original = wavfile.read("secondchance.wav")

	channels = len(original.shape)

	if channels == 2:
		t = original.transpose()
		l = t[0]
		r = t[1]
		original = (-l / 2) + (r / 2)

	sample_length = 1024

	result = zeros( (len(original)), dtype=int16)

	synth_num = 4
	synths = []

	for i in xrange(synth_num):
		synths.append(FartSynthesizer(44100, 1024))

	for s in xrange(0, len(original), sample_length):
		tone = original[s:s+sample_length]
		if len(tone) != sample_length:
			break

		print "\rLocation:", s,

		fft = abs(rfft(tone))

		peaks = find_peaks(fft)
		peaks.sort(key=lambda p: fft[p], reverse=True)

		for i in xrange(min(synth_num, len(peaks))):
			freq = (peaks[i] * rate) / sample_length
			amp = fft[peaks[i]] / sample_length

			# synths[i].freq = (synths[i].freq * 0.1) + (freq * 0.9)
			# synths[i].amp = (synths[i].amp * 0.1) + (amp * 0.9)

			synths[i].freq = freq
			synths[i].amp = amp

			result[s:s+sample_length] += synths[i].generate()

	print "Done"

	wavfile.write("out.wav", rate, result)

	play(result / max(result))

if __name__ == '__main__':
	main()